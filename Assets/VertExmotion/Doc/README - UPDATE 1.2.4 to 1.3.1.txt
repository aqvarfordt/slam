How to Update VertExmotion 1.2.x to 1.3.1
(VertExmotion BASIC only)

VertExmotion 1.3.1 iclude a new structure of scripts.
Open Update tools 'Menu/Window/VertExmotion/VertExmotion Updater'

UPDATE PREFABS
- Create a new scene.
- Instantiate your prefab using VertExmotion.
- Click 'Scan current scene'
- Click 'Update Scripts'
- Select each prefab and Apply changes


UPDATE SCENES
- Open the scene
- Click 'Scan current scene'
- Click 'Update Scripts'
- Save the scene


FINALIZE UPDATE
When all the scenes are updates:
- Delete 'Assets/VertExmotion/VertExmotion.dll'
- Delete 'Assets/VertExmotion/Editor/VertExmotionEditor.dll'


