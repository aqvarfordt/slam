﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Reflection;

using UnityObject = UnityEngine.Object;

[CanEditMultipleObjects]
[CustomEditor( typeof( MonoBehaviour ) )]
public class VertExmotionPatcherCustomEditor : Editor
{



	public override void OnInspectorGUI()
	{			
		var scriptProperty = this.serializedObject.FindProperty ("m_Script");
		if (scriptProperty == null || scriptProperty.objectReferenceValue != null)
		{

			GameObject go = (target as Component).gameObject;
			//GUILayout.Label (">" + scriptProperty.objectReferenceValue.name);


						var types = Resources
				.FindObjectsOfTypeAll (typeof(MonoScript))
					.Where (x => x.name == scriptProperty.objectReferenceValue.name)
					.Where (x => x.GetType () == typeof(MonoScript)) // Fix for Unity crash
					.Cast<MonoScript> ()
						//.Select( x => x.GetClass() != null )
					.Where (x => x.GetClass () != null)
						///.Where( x => x.GetClass().Assembly.FullName.Split(',')[0] == "VertExmotion" )//check only VertExmotion DLL
					.ToList (); 

						//for( int scr=0; scr<types.Count; ++scr )
						//GUILayout.Label( types[scr].GetClass().Assembly.FullName );

						if (types.Count > 0) {
								VertExmotionUpdater window = (VertExmotionUpdater)EditorWindow.GetWindow (typeof(VertExmotionUpdater));
								window.Show ();
						}
			
						if (types.Count == 1) {

								

								if (VertExmotionUpdater.m_currentGameobject == go) {
										scriptProperty.objectReferenceValue = types [0];
										serializedObject.ApplyModifiedProperties ();
										VertExmotionUpdater.m_currentGameobjectConverted = true;
										return;

								}

						}
//				if( GUILayout.Button("convert") )
//				{
//
//						scriptProperty.objectReferenceValue = types[0];
//						serializedObject.ApplyModifiedProperties();
//
//				}


				EditorGUILayout.HelpBox ("Outdated vertion of " + scriptProperty.objectReferenceValue.name + "\nPlease Convert the scene", MessageType.Warning);

			if( isPrefab(go) )
				EditorGUILayout.HelpBox ("Instantiate this prefab in a new scene, then convert the scene and apply prefab modifications", MessageType.Warning);

						//base.OnInspectorGUI();
						return;
				}
		else
		{
			base.OnInspectorGUI();
		}
		}



	
	private static bool isPrefab( GameObject item )
	{
		
		if( item == null )
			return false;
		
		return
			item != null &&
				PrefabUtility.GetPrefabParent( item ) == null &&
				PrefabUtility.GetPrefabObject( item ) != null;
		
	}

}