﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Reflection;

using UnityObject = UnityEngine.Object;


/*
[InitializeOnLoad]
public class LaunchVertExmotionUpdater
{
	static LaunchVertExmotionUpdater()
	{
		if (Application.isPlaying)
			return;


		List<MonoScript> scripts = Resources
			.FindObjectsOfTypeAll( typeof( MonoScript ) )
				.Where( x => x != null )
				.Where( x => x.GetType() == typeof( MonoScript ) )
				.Where( x => x.name.Contains("VertExmotion") )
				.Cast<MonoScript>()
				.Where( x => x.GetClass() == null )
				//.Where( x => x.GetClass().Assembly.FullName.Split(',')[0] == "VertExmotion" )//check only VertExmotion DLL
				.ToList();


		//old DLL found launch updater
		if( scripts.Count > 0 )
			VertExmotionUpdater.Init ();

	}

}*/

public class VertExmotionUpdater : EditorWindow {
	[MenuItem ("Window/VertExmotion/1.2.x->1.3.1 Updater")]
	public static void Init () {
		// Get existing open window or if none, make a new one:
		VertExmotionUpdater window = (VertExmotionUpdater)EditorWindow.GetWindow (typeof (VertExmotionUpdater));
		window.Show();



	}

	public static string m_message = "";
	public static GameObject m_currentGameobject;
	public static bool m_currentGameobjectConverted = false;


	public class SwitchInfo
	{
		public string className;
		public System.Type type_cSharp = null;
		public System.Type type_dll = null;

		public SwitchInfo( string name ){ className = name; }
		
	}



	private static MonoScript getScriptFromType( System.Type t )
	{

		Debug.Log ("getScriptFromType");


		List<MonoScript> scripts = Resources
			.FindObjectsOfTypeAll( typeof( MonoScript ) )
				.Where( x => x.name == t.Name.Replace("Kalagaan.", "") )
				.Where( x => x.GetType() == typeof( MonoScript ) )
				.Where( x => x.name.Contains("VertExmotion") )
				.Cast<MonoScript>()
				//.Select( x => new ScriptMatcher( x ) )
				//.Where( x => x.Type != null && !x.Type.IsAbstract && !x.Type.IsGenericType )
				.Where( x => x.GetClass().Assembly.FullName.Split(',')[0] != "VertExmotion" )//check only VertExmotion not DLL
				.ToList();


		for (int i=0; i<scripts.Count; ++i)
			Debug.Log (" >> " + scripts [i].name);


		if( scripts.Count == 0 )
			return null;

		return scripts[0];
		
	}


	private static List<MonoScript> getDefinedScriptTypes()
	{
		
		if( types != null )
			return types;
		
		types = Resources
			.FindObjectsOfTypeAll( typeof( MonoScript ) )
				.Where( x => x.GetType() == typeof( MonoScript ) )
				.Where( x => x.name.Contains("VertExmotion") )
				.Cast<MonoScript>()
				.ToList();

		return types;
		
	}


	private static List<MonoScript> types = null;

	public bool m_startConversion = false;

	public static List<SwitchInfo> m_infos = null;
	public List<GameObject> m_scriptTarget = new List<GameObject>();


	void OnGUI () {
		GUILayout.Label ("VertExmotion Updater\n"
		                 + " 1.2.x -> 1.3.x "
		                 , EditorStyles.boldLabel);

		//m_target = EditorGUILayout.ObjectField ("target", m_target, typeof(Kalagaan.Vertexmotion), true);

		EditorGUILayout.HelpBox( "Please open README file for Update to 1.3.x\n( 'VertExmotion/Doc' )", MessageType.Warning );



		if( m_infos == null )
		{
			m_infos = new List<SwitchInfo>();
			m_infos.Add( new SwitchInfo( "VertExmotionCollider" ) );
			m_infos.Add( new SwitchInfo( "VertExmotionSensor" ) );
			m_infos.Add( new SwitchInfo( "VertExmotion" ) ) ;


		}


		if( GUILayout.Button ( "Scan current scene" ) )
		{
			m_scriptTarget.Clear();

			m_scriptTarget = FindObjectsOfType<GameObject>()
				.Where( x => x.GetComponents<Component>()
				       //.Any( c => c!= null && c.GetType().Name.Contains("VertExmotion") && c.GetType().Assembly.FullName.Split (',')[0] == "VertExmotion" ) )
				       .Any( c => c== null ) )
					.ToList();


			if( m_scriptTarget.Count == 0 )
				m_message = "No outdated VertExmotion script found in this scene";
			else
			{
				m_message = "" + m_scriptTarget.Count + " script(s) found, scene need to be updated.";

//				Component cpn = m_scriptTarget
//					.Where( x => x.GetComponents<Component>()
//					       //.Any( c => c!= null && c.GetType().Name.Contains("VertExmotion") && c.GetType().Assembly.FullName.Split (',')[0] == "VertExmotion" ) )
//					       .Any( c => c== null ) )
//						.Select( c => c.GetComponent("Kalagaan.VertExmotion") )
//						.ToList()[0];

			}

		}	


		for( int i=0; i<m_scriptTarget.Count; ++i )
		{
			if(  m_scriptTarget[i] != null )
				GUILayout.Label( ""+m_scriptTarget[i].name );

		}




		if( m_startConversion )
		{
			if( m_scriptTarget.Count > 0 )
			{
				if( Selection.activeObject != m_scriptTarget[0] )
				{
					m_currentGameobject = m_scriptTarget[0];
					m_currentGameobjectConverted = false;
					Selection.activeObject = m_scriptTarget[0];
				}


				if( m_currentGameobjectConverted )
				{
					m_scriptTarget.RemoveAt( 0 );
				}
			}
			else
			{
				m_startConversion = false;
				m_message = "Conversion done.\nSave current scene.";			
				m_scriptTarget.Clear();
				Selection.activeObject = null;

			}

		}


		if( m_scriptTarget.Count > 0 )
		{
			if( GUILayout.Button("Update Scripts") )
			{
				m_startConversion = true;
				Selection.activeObject = null;
			}
		}

		if (m_message != "")
			GUILayout.Label (m_message, EditorStyles.helpBox);


	}




}
