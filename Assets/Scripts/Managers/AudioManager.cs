﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
	public static AudioManager Instance;

	public AudioMixer AudioMixer;
	public AudioSource MusicSource;
	public AudioSource MusicEffectsSource;
	public AudioSource EffectsSource;

	public AudioClip ChooseSong;
	public AudioClip[] Songs;
	public AudioClip[] Effects;

	public float SmoothRate = 0.1f;
	public float MusicEffectsVolume;

	[Range(0, 1)]
	public float NormalizedVolume;

	[HideInInspector]
	public AudioListener MainAudioListener;

	private const int QSamples = 128; // array size
	private const float RefValue = 0.1f; // RMS value for 0 dB
	float _rmsValue;   // sound level - RMS
	float _dbValue;    // sound level - dB
	private float[] _samples;
	float[] _musicData;

	float _smoothVelocity;

	float _minVol = 0;
	float _maxVol = 0.1f;

	void Awake()
	{
		if (Instance == null)
			Instance = this;

		MainAudioListener = Camera.main.GetComponent<AudioListener>();
	}

	void Start()
	{
		_samples = new float[QSamples];

		if (ChooseSong == null)
			RandomizeSong();
		else
			MusicSource.clip = ChooseSong;

		MusicEffectsSource.clip = MusicSource.clip;
		MusicSource.Play();
		MusicEffectsSource.Play();
	}

	void Update()
	{
		MusicEffectsVolume = GetVolume(MusicEffectsSource);

		if (MusicEffectsVolume > _maxVol)
			_maxVol = MusicEffectsVolume;
		if (MusicEffectsVolume < _minVol && MusicEffectsVolume > 0)
			_minVol = MusicEffectsVolume;

		NormalizedVolume = Mathf.SmoothDamp(NormalizedVolume, MusicEffectsVolume / _maxVol, ref _smoothVelocity, 0.1f);

		if (Input.GetKeyDown("t"))
		{
			RandomizeSong();
		}
	}

	public IEnumerator SlamDistort(float volumeLevel = 1)
	{
		if (EffectsSource.isPlaying)
			EffectsSource.Stop();

		AudioMixer.SetFloat("MusicDistortion", 0.8f);

		float distortLevel = 0.8f;

		EffectsSource.volume = volumeLevel;
		EffectsSource.clip = SwitchClipOnVolume(volumeLevel);
		EffectsSource.Play();

		yield return null;

		while (distortLevel > 0)
		{
			AudioMixer.GetFloat("MusicDistortion", out distortLevel);
			AudioMixer.SetFloat("MusicDistortion", distortLevel - 0.05f);

			yield return new WaitForFixedUpdate();
		}
	}

	public void Boost()
	{
		if (EffectsSource.isPlaying)
			EffectsSource.Stop();

		EffectsSource.volume = 1.5f;
		EffectsSource.clip = Effects[2];
		EffectsSource.Play();
	}

	public float GetVolume(AudioSource source)
	{
		source.GetOutputData(_samples, 0); // fill array with samples
		int i;
		float sum = 0;

		for (i = 0; i < QSamples; i++)
		{
			sum += _samples[i] * _samples[i]; // sum squared samples
		}

		_rmsValue = Mathf.Sqrt(sum / QSamples); // rms = square root of average
		_dbValue = 20 * Mathf.Log10(_rmsValue / RefValue); // calculate dB
		if (_dbValue < -160) _dbValue = -160; // clamp it to -160dB min

		if (_dbValue < 0)
			_dbValue = 0;

		return _dbValue;
	}

	AudioClip SwitchClipOnVolume(float volumeLevel)
	{
		return volumeLevel < 1f ? Effects[0] : Effects[1];
	}

	public void RandomizeSong()
	{
		MusicSource.clip = Songs[Random.Range(0, Songs.Length)];
		MusicEffectsSource.clip = MusicSource.clip;
		MusicSource.Play();
		MusicEffectsSource.Play();
	}
}
