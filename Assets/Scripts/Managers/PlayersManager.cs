﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.Networking;

public class PlayersManager : NetworkBehaviour
{
	public static PlayersManager Instance;

	public readonly int MaxPlayers = 2;
	static public List<RobotManager> Robots = new List<RobotManager>();             // A collection of managers for enabling and disabling different aspects of the tanks.
	public Transform[] SpawnPoints;

	public GameObject PlayersPanel;
	public GameObject PlayerSignPrefab;
	public GameObject TrackerArrow;

	public GameObject[] PlayerPrefabs;
	public static List<GameObject> Players = new List<GameObject>();
	public static List<int> PlayerDeaths = new List<int>();

	[SyncVar]
	public bool m_GameIsFinished = false;

	private readonly List<GameObject> _playersUi = new List<GameObject>();
	private readonly List<GameObject> _playerTracker = new List<GameObject>();
	private readonly List<GameObject> _playerEnergyUi = new List<GameObject>();

	void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
	}

	//void Start()
	//{
	//	PlayerJoin();
	//}

	//void Update()
	//{
	//	if (Input.GetKeyDown("y"))
	//	{
	//		if (Players.Count == 0)
	//			return;

	//		RemovePlayer(Players.Count);
	//	}
	//	if (Input.GetKeyDown("u"))
	//	{
	//		PlayerJoin();
	//	}
	//}

	static public void AddRobot(GameObject robot, int playerNum, Color c, string name, int localID)
	{
		RobotManager tmp = new RobotManager();
		tmp.m_Instance = robot;
		tmp.m_PlayerNumber = playerNum;
		tmp.m_PlayerColor = c;
		tmp.m_PlayerName = name;
		tmp.m_LocalPlayerID = localID;
		tmp.Setup();

		Robots.Add(tmp);
	}

	public void RemoveRobot(GameObject tank)
	{
		RobotManager toRemove = null;
		foreach (var tmp in Robots)
		{
			if (tmp.m_Instance == tank)
			{
				toRemove = tmp;
				break;
			}
		}

		if (toRemove != null)
			Robots.Remove(toRemove);
	}

	//public void PlayerJoin()
	//{
	//	if (Players.Count > 5)
	//		return;

	//	var newPlayer = Instantiate(PlayerPrefabs[0]);
	//	var newSign = Instantiate(PlayerSignPrefab);
	//	var signText = newSign.GetComponent<Text>();
	//	var trackerArrow = Instantiate(TrackerArrow);
	//	var playerController = newPlayer.GetComponent<PlayerController>();
	//	var trackerScript = trackerArrow.GetComponent<TrackingArrow>();

	//	PlayerDeaths.Add(0);
	//	Players.Add(newPlayer);
	//	newSign.transform.SetParent(PlayersPanel.transform);
	//	playerController.PlayerId = Players.Count;

	//	_playersUi.Add(newSign);
	//	_playerTracker.Add(trackerArrow);
	//	_playerEnergyUi.Add(playerController.EnergyBarInstance);

	//	newPlayer.transform.SetParent(transform);
	//	newPlayer.transform.localPosition = SpawnPosition();
	//	signText.text = "Player " + Players.Count;
	//	trackerScript.Initialize(newPlayer.transform);
	//}

	public void RemovePlayer(int playerId)
	{
		var player = Players[playerId - 1];
		var playerUi = _playersUi[playerId - 1];
		var playerTracker = _playerTracker[playerId - 1];
		var energyUi = _playerEnergyUi[playerId - 1];

		Players.Remove(player);
		_playerTracker.Remove(playerTracker);
		_playersUi.Remove(playerUi);
		_playerEnergyUi.Remove(energyUi);

		Destroy(playerTracker);
		Destroy(playerUi);
		Destroy(player);
		Destroy(energyUi);
	}

	public void PlayerDeath(int playerId)
	{
		var player = Players[playerId - 1];
		var playerUi = _playersUi[playerId - 1];
		var playerTracker = _playerTracker[playerId - 1];
		var energyUi = _playerEnergyUi[playerId - 1];

		PlayerDeaths[playerId - 1] += 1;
		playerUi.transform.FindChild("Panel/Text").GetComponent<Text>().text = PlayerDeaths[playerId - 1].ToString();
		player.GetComponent<Rigidbody>().velocity = Vector3.zero;
		player.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;

		playerTracker.SetActive(false);
		player.SetActive(false);
		energyUi.SetActive(false);

		StartCoroutine(PlayerRespawn(playerId));
	}

	IEnumerator PlayerRespawn(int playerId)
	{
		yield return new WaitForSeconds(1);

		var player = Players[playerId - 1];
		var playerUi = _playersUi[playerId - 1];
		var playerTracker = _playerTracker[playerId - 1];
		var energyUi = _playerEnergyUi[playerId - 1];

		var playerController = player.GetComponent<PlayerController>();

		player.transform.position = SpawnPosition();
		playerTracker.SetActive(true);
		playerUi.SetActive(true);
		player.SetActive(true);
		energyUi.SetActive(true);

		playerController.SetEnergyLevel(100);
	}

	int GetIndexOfPlayer(GameObject player)
	{
		return Players.IndexOf(player);
	}

	Vector3 SpawnPosition()
	{
		return Vector3.zero;
	}
}
