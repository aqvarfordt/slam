﻿using UnityEngine;
using System.Collections;

public class ParticleManager : MonoBehaviour
{

    public static ParticleManager Instance;

    public ParticleSystem ParticleTest;
    public ParticleSystem BounceFloor;
	public ParticleSystem Slam;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void PlayEffect(ParticleSystem particles, Vector3 location)
    {
        var particleEffect = Instantiate(particles);
        particleEffect.transform.position = location;
    }
}
