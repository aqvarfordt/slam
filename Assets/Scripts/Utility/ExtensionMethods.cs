﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public static class ExtensionMethods {

	public static Vector3 GetScreenPosition(this Transform transform, Canvas canvas, Camera cam)
	{
		var width = canvas.GetComponent<RectTransform>().sizeDelta.x;
		var height = canvas.GetComponent<RectTransform>().sizeDelta.y;
		var x = Camera.main.WorldToScreenPoint(transform.position).x / Screen.width;
		var y = Camera.main.WorldToScreenPoint(transform.position).y / Screen.height;
		var pos = new Vector3(width * x, y * height);
		return pos;
	}

	public static Color HexToColor(string hex)
	{
		byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
		byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
		byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
		byte a = byte.Parse(hex.Substring(6, 4), System.Globalization.NumberStyles.HexNumber);
		return new Color32(r, g, b, a);
	}

	public static IEnumerator LerpSliderValue(this Slider slider,float from, float to, float duration)
	{

		var startTime = Time.time;
		float progress = 0;

		if (to >= 100)
		{
			to = 100;
		}
		if (to <= 0)
		{
			to = 0;
		}

		while (progress < 1)
		{
			progress = (Time.time - startTime) / duration;
			slider.value = Mathf.Lerp(from, to, progress);

			yield return new WaitForFixedUpdate();
		}

	} 
}
