﻿using UnityEngine;
using System.Collections;

public class UiFollowGameObject : MonoBehaviour
{
	public Vector3 Offset;
	public bool Smooth;
	public float SmoothRate;

	private Transform _target;
	private Canvas _canvas;
	private Camera _mainCamera;

	private Vector3 _smoothVelocity;

	public void Initialize(Transform target)
	{
		
		_canvas = transform.root.GetComponent<Canvas>();
		_mainCamera = Camera.main;
		_target = target;
	}

	void Update()
	{
		if (!_target)
			return;

		var targetPos = _target.GetScreenPosition(_canvas, _mainCamera) + Offset;

		transform.position = !Smooth ? targetPos : Vector3.SmoothDamp(transform.position, targetPos, ref _smoothVelocity, SmoothRate);
	}
}
