﻿using UnityEngine;
using System.Collections;

public class TrackingArrow : MonoBehaviour
{
	public Transform _target;

	public Vector2 XClamp;
	public Vector2 ZClamp;

	public Color BottomColor;
	public Color TopColor;

	public float LowestPoint = -5;
	public float HighestPoint = 2;

	[SerializeField]
	private float distance = 10.0f;
	// the height we want the camera to be above the _target
	[SerializeField]
	private float height = 5.0f;
	[SerializeField]
	private float rotationDamping;
	[SerializeField]
	private float heightDamping;

	private float _adjustedLowestPoint;
	private float _adjustedHighestPoint;
	public float _normalizedHeight;
	private Renderer _renderer;
	private bool _visible = true;

	public void Initialize(Transform target)
	{
		_adjustedHighestPoint = HighestPoint + Mathf.Abs(LowestPoint);
		_renderer = GetComponent<Renderer>();
		_target = target;
		gameObject.SetActive(true);
	}

	void Update()
	{
		if (_visible)
			return;

		_visible = ShowArrow();
	}

	void LateUpdate()
	{
		// Early out if we don't have a _target
		if (!_visible)
			return;

		_renderer.enabled = ShowArrow();

		_normalizedHeight = (_target.position.y + Mathf.Abs(LowestPoint)) /_adjustedHighestPoint;
		_renderer.material.color = Color.Lerp(BottomColor, TopColor, _normalizedHeight);

		// Calculate the current rotation angles
		var wantedRotationAngle = _target.eulerAngles.y;
		var wantedHeight = _target.position.y + height;

		var currentRotationAngle = transform.eulerAngles.y;
		var currentHeight = transform.position.y;

		// Damp the rotation around the y-axis
		currentRotationAngle = Mathf.LerpAngle(currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);

		// Damp the height
		currentHeight = Mathf.Lerp(currentHeight, wantedHeight, heightDamping * Time.deltaTime);

		// Convert the angle into a rotation
		var currentRotation = Quaternion.Euler(0, currentRotationAngle, 0);

		// Set the position of the camera on the x-z plane to:
		// distance meters behind the _target
		transform.position = _target.position;
		transform.position -= currentRotation * Vector3.forward * distance;

		var clampXPos = Mathf.Clamp(transform.position.x, XClamp.x, XClamp.y);
		var clampZPos = Mathf.Clamp(transform.position.z, ZClamp.x, ZClamp.y);

		// Set the height of the camera
		transform.position = new Vector3(clampXPos, currentHeight, clampZPos);

		// Always look at the _target
		transform.LookAt(_target);

		_visible = ShowArrow();
	}

	bool ShowArrow()
	{
		var returnBool = _target.position.x < XClamp.x || _target.position.x > XClamp.y || _target.position.z < ZClamp.x || _target.position.z > ZClamp.y;

		return returnBool;
	}
}
