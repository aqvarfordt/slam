﻿using UnityEngine;
using System.Collections;

public class DestroySelf : MonoBehaviour {

    public float DestroyTime = 2;

    void Awake()
    {
        Invoke("DestroyObject", DestroyTime);
    }

    void DestroyObject ()
    {
        Destroy(gameObject);
    }

}
