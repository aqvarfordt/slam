﻿using UnityEngine;
using System.Collections;

public class LerpColor : MonoBehaviour
{

	public Color ColorFrom;
	public Color ColorTo;

	public Color NewColor;

	private Renderer _renderer;

	void Start()
	{
		_renderer = GetComponent<Renderer>();
	}

	void Update ()
	{
		NewColor = Color.Lerp(ColorFrom, ColorTo, AudioManager.Instance.NormalizedVolume);
        _renderer.material.color = NewColor;
	}
}
