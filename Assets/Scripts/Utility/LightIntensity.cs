﻿using UnityEngine;
using System.Collections;

public class LightIntensity : MonoBehaviour
{
	public float IntensityMultiplier;
	private Light _light;

	void Start ()
	{
		_light = GetComponent<Light>();
	}
	
	void Update ()
	{
		_light.intensity = AudioManager.Instance.NormalizedVolume * IntensityMultiplier;
	}
}
