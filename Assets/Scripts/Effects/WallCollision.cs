﻿using UnityEngine;
using System.Collections;

public class WallCollision : MonoBehaviour
{
	public float HitForce;

    private void OnCollisionEnter(Collision collision)
    {
		if (!ColliderIsWall(collision))
            return;

        var playerController = collision.gameObject.GetComponent<PlayerController>();
        var contact = collision.contacts[0];
        var particleTest = Instantiate(ParticleManager.Instance.ParticleTest);

        

        StartCoroutine(AudioManager.Instance.SlamDistort(collision.gameObject.GetComponent<PlayerController>().NormalizedVelocity));
		collision.rigidbody.AddForce(-contact.normal * (playerController.NormalizedVelocity * HitForce));
		playerController.HeadPivot.GetComponent<Rigidbody>().AddExplosionForce(playerController.NormalizedVelocity * HitForce, contact.point, 50);


		// Emitting burst particles instead of regulating emission with emission rate
		// TODO: Instantiate particle system as a class var instead of making multiple accessor calls
		particleTest.transform.GetChild(0).GetComponent<ParticleSystem>().emissionRate = 0;
		particleTest.transform.GetChild(0).GetComponent<ParticleSystem>().Emit (10 * (int)collision.relativeVelocity.magnitude);

		// Basing particle start speed on the collision magnitude
		// TODO: Experiment with the start size and other kewl variables, maybe add a touch of reduced particle lifetime for small collisions
		particleTest.transform.GetChild(0).GetComponent<ParticleSystem>().startSpeed = -1 + ((float)collision.relativeVelocity.magnitude / 3);
        particleTest.transform.position = new Vector3(contact.point.x, contact.point.y, contact.point.z);
        particleTest.transform.rotation = Quaternion.LookRotation(-contact.normal) ;
    }

    bool ColliderIsWall (Collision collision)
    {
        foreach (ContactPoint contactPoint in collision.contacts)
        {
            if (contactPoint.thisCollider.name == "WallCollider")
                return true;
        }
        return false;
    }

	bool ColliderIsPlatform(Collision collision)
	{
		foreach (ContactPoint contactPoint in collision.contacts)
		{
			if (contactPoint.thisCollider.name == "Cube")
				return true;
		}
		return false;
	}
}