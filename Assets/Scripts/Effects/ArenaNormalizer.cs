﻿using UnityEngine;
using System.Collections;

public class ArenaNormalizer : MonoBehaviour {

	Vector3 CurrentPos;

	private const float PositionDampingFactor = 0.97f;
	private const float RotationDampingFactor = 0.94f;

	private Vector3 currentPos;
	private Quaternion currentRot;

	void Update () {
		Dampen();
	}

	private void Dampen()
	{
		currentPos = gameObject.transform.position;
		currentRot = gameObject.transform.rotation;

		Vector3 positionDampen = new Vector3( currentPos.x * PositionDampingFactor, -1, currentPos.z * PositionDampingFactor);
		Quaternion rotationDampen = new Quaternion(currentRot.x * RotationDampingFactor, currentRot.y, currentRot.z * RotationDampingFactor, currentRot.w);

		gameObject.transform.position = positionDampen;
		gameObject.transform.rotation = rotationDampen;
	}
}
