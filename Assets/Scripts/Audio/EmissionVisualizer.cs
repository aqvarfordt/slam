﻿using UnityEngine;
using System.Collections;

public class EmissionVisualizer : MonoBehaviour {

    Material _visMaterial;
    Color _newColor = new Color();

    void Start ()
    {
        _visMaterial = GetComponent<Renderer>().material;
    }
 
    void Update ()
    {

		_newColor.r = AudioManager.Instance.NormalizedVolume;
		_newColor.g = AudioManager.Instance.NormalizedVolume;
		_newColor.b = AudioManager.Instance.NormalizedVolume;

		_visMaterial.SetColor("_EmissionColor", _newColor);
    }
}
