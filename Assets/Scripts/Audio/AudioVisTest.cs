﻿using UnityEngine;
using System.Collections;

public class AudioVisTest : MonoBehaviour {

    public GameObject Item;

    public float smoothValue;

	// Update is called once per frame
	void Update () {

        smoothValue = AudioManager.Instance.GetVolume(AudioManager.Instance.MusicEffectsSource) * Random.Range(0.4f, 0.6f);

        Item.transform.localPosition = new Vector3(Item.transform.localPosition.x, Item.transform.localPosition.y, smoothValue);
	}
}
