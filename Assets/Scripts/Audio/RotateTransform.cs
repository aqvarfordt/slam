﻿using UnityEngine;
using System.Collections;

public class RotateTransform : MonoBehaviour
{

	public float RotateSpeed;
	public Vector3 Axis;

	void Update()
	{
		transform.Rotate(Axis, RotateSpeed + (AudioManager.Instance.GetVolume(AudioManager.Instance.MusicEffectsSource) * 0.05f) * Time.deltaTime);

	}

}
