﻿using UnityEngine;
using System.Collections;

public class GroundCheck : MonoBehaviour {

    public PlayerController Controller;

    void OnTriggerExit (Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            Controller.Grounded = false;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            Controller.Grounded = true;
        }
    }

}
