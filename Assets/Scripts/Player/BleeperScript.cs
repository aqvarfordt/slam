﻿using UnityEngine;
using System.Collections;

public class BleeperScript : MonoBehaviour
{
	public Color BleeperColor;

	public float FadeRate;
	public float RepeatRate;
	public float PeakIntensity;

	private Color _startColor;
	private Light _bleepLight;
	private Renderer _renderer;

	void Awake()
	{
		_bleepLight = GetComponentInChildren<Light>();
		_renderer = GetComponent<Renderer>();
		_startColor = _renderer.material.color;
		_bleepLight.color = BleeperColor;
	}

	void Start()
	{
		InvokeRepeating("RunBleep", RepeatRate, RepeatRate);
	}

	public void RunBleep()
	{
		if (!gameObject.activeSelf)
			return;

			StartCoroutine(Bleep());
	}

	public IEnumerator Bleep()
	{
		yield return null;
         Color newColor = BleeperColor;
        _bleepLight.intensity = PeakIntensity;
		while (_bleepLight.intensity > 0)
		{
			if (!gameObject.activeSelf)
				yield break;

			_bleepLight.intensity -= 0.1f;
		    newColor.a = _bleepLight.intensity;
		    _renderer.material.color = newColor;
			yield return new WaitForSeconds(FadeRate);
		}
		yield return null;
	    _renderer.material.color = _startColor;
	}
}
