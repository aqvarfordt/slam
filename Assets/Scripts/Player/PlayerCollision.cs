﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerCollision : NetworkBehaviour
{
	public float ImpactModifier;

	private PlayerController _myController;
	private PlayerController _theirController;

	void Awake()
	{
		_myController = GetComponent<PlayerController>();
	}

	[ServerCallback]
	private void OnCollisionEnter(Collision collision)
	{
		if (!ColliderIsPlayer(collision))
			return;

		var contact = collision.contacts[0];
		var particleTest = Instantiate(ParticleManager.Instance.Slam);
		particleTest.transform.position = contact.point;


		_theirController = collision.gameObject.GetComponent<PlayerController>();

		StartCoroutine(AudioManager.Instance.SlamDistort(_myController.NormalizedVelocity));

		_theirController.Rigidbody.AddForce(_myController.Acceleration * _myController.Rigidbody.velocity.magnitude * ImpactModifier);
	}

	bool ColliderIsPlayer(Collision collision)
	{
		foreach (ContactPoint contactPoint in collision.contacts)
		{
			if (contactPoint.otherCollider.tag == "Player")
				return true;
		}
		return false;
	}

	//void PhysicForces()
	//{
	//	// Collect all the colliders in a sphere from the shell's current position to a radius of the explosion radius.
	//	Collider[] colliders = Physics.OverlapSphere(transform.position, m_ExplosionRadius, m_TankMask);

	//	// Go through all the colliders...
	//	for (int i = 0; i < colliders.Length; i++)
	//	{
	//		// ... and find their rigidbody.
	//		Rigidbody targetRigidbody = colliders[i].GetComponent<Rigidbody>();

	//		// If they don't have a rigidbody or we don't own that object, go on to the next collider.
	//		if (!targetRigidbody || !targetRigidbody.GetComponent<NetworkIdentity>().hasAuthority)
	//			continue;

	//		// Add an explosion force with no vertical bias.
	//		targetRigidbody.AddExplosionForce(m_ExplosionForce, transform.position, m_ExplosionRadius);
	//	}
	//}

}
