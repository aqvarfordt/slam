﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerEngine : NetworkBehaviour
{

	public int m_PlayerNumber;
	public int m_LocalID;

	public float SpeedModifier;
	public float MaxVelocity;
	public bool ApplyingThrottle;
	public int PlayerId = 1;
	public Vector3 Acceleration;
	public ParticleSystem BoostParticles;
	public GameObject Trail;


	[Range(0, 1.5f)]
	public float NormalizedVelocity;

	public bool Grounded;

	[HideInInspector]
	public GameObject HeadPivot;
	[HideInInspector]
	public Rigidbody Rigidbody;
	[HideInInspector]
	public float CurrentEnergy = 100;
	[HideInInspector]
	public readonly float MaxEnergy = 100;
	[HideInInspector]
	public readonly float ChargeRate = 0.1f;
	[HideInInspector]
	public GameObject EnergyBarInstance;

	protected float HorizontalAxis;
	protected float VerticalAxis;

	private BoxCollider _groundCheck;
	private GameObject _ball;
	private Vector3 _lateUpdate;

	public GameObject EnergyBarContainer;
	public GameObject EnergyBar;

	public Color HighEnergyColor;
	public Color LowEnergyColor;

	private Slider _energySlider;
	private Image _fillArea;
	private Image _backgroundArea;
	private bool _initialized;
	private bool _isBarVisible = true;
	private bool _allowCharge = true;
	public bool _allowBoost = true;
	private Color _barColor;

	void Awake()
	{
		transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
		_ball = transform.FindChild("Mesh/Ball").gameObject;
		_lateUpdate = transform.position;
		HeadPivot = transform.FindChild("Mesh/HeadPivot").gameObject;
		Rigidbody = GetComponent<Rigidbody>();

		LeanTween.scale(gameObject, new Vector3(1f, 1f, 1f), 0.4f).setEase(LeanTweenType.easeInOutElastic);

		EnergyBarInstance = Instantiate(EnergyBar);
		EnergyBarInstance.transform.SetParent(GameObject.Find("Canvas/Panel/").transform);
		EnergyBarInstance.GetComponent<UiFollowGameObject>().Initialize(transform);

		_energySlider = EnergyBarInstance.GetComponentInChildren<Slider>();
		_fillArea = EnergyBarInstance.transform.FindChild("Slider/Fill Area/Fill").GetComponent<Image>();
		_backgroundArea = EnergyBarInstance.transform.FindChild("Slider/Background").GetComponent<Image>();
		_energySlider.maxValue = MaxEnergy;
	}

	public void FixedUpdate()
	{
		if (!isLocalPlayer)
			return;

		Acceleration = transform.position - _lateUpdate;
		_lateUpdate = transform.position;
		Accelerate();
		HeadPivot.GetComponent<Rigidbody>().AddForce(new Vector3(-Acceleration.z, 0, -Acceleration.x) * 20);
		NormalizedVelocity = GetNormalizedVelocity();

		ChargeEnergy();
		CheckToggleEnergyUi();
	}

	public void SetEnergyLevel(float amount)
	{
		StopCoroutine("ExtensionMethods.LerpSliderValue");
		CurrentEnergy = amount;
		_energySlider.value = amount;
	}

	public void Accelerate()
	{
		if (Rigidbody.velocity.magnitude < MaxVelocity)
			Rigidbody.AddForce(HorizontalAxis * SpeedModifier, 0, VerticalAxis * SpeedModifier);
	}

	public void JumpTest()
	{
		Rigidbody.velocity = new Vector3(Rigidbody.velocity.x, 7, Rigidbody.velocity.z);
	}

	public void ChangeEnergyLevel(float amount)
	{
		_allowCharge = false;
		const float animTime = 0.4f;

		StartCoroutine(_energySlider.LerpSliderValue(_energySlider.value, _energySlider.value + amount, animTime));
		CurrentEnergy += amount;
		Invoke("ChangeEnergyColorGrey", animTime);
		Invoke("AllowCharge", 2);
	}

	void ChangeEnergyColorGrey ()
	{
		_fillArea.color = Color.grey;
	}

	void AllowCharge()
	{ 
		_allowCharge = true;
	}

	void AllowBoost()
	{
		_allowBoost = true;
	}

	protected void Boost()
	{
		CancelInvoke("AllowBoost");
		AudioManager.Instance.Boost();
		BoostParticles.Play();
		ChangeEnergyLevel(-20);
		Rigidbody.AddForce(new Vector3(HorizontalAxis, 0, VerticalAxis) * 500);
		Trail.SetActive(true);
		Trail.GetComponent<TrailRenderer>().startWidth = 2;
		StartCoroutine(FadeTrail());
		Invoke("AllowBoost", 2);
	}

	float GetNormalizedVelocity()
	{
		var adjustedBallRotVelocity = new Vector3(-Rigidbody.velocity.z, 0, -Rigidbody.velocity.x);
		_ball.transform.Rotate(adjustedBallRotVelocity, Space.Self);
		return Rigidbody.velocity.magnitude * 0.1f;
	}

	protected void CheckToggleEnergyUi()
	{
		if (CurrentEnergy < MaxEnergy && !_isBarVisible)
		{
			_isBarVisible = true;
			StartCoroutine(ToggleBarAnim(0, 1));
		}
		else if (CurrentEnergy >= MaxEnergy && _isBarVisible)
		{
			_isBarVisible = false;
			StartCoroutine(ToggleBarAnim(1, 0));
		}
	}

	void ChargeEnergy()
	{
		if (CurrentEnergy >= MaxEnergy)
		{
			_allowCharge = false;
		}

		if (!_allowCharge)
			return;

		CurrentEnergy += ChargeRate;
		_energySlider.value = CurrentEnergy;
		_barColor = Color.Lerp(LowEnergyColor, HighEnergyColor, CurrentEnergy / MaxEnergy);
		_barColor.a = _fillArea.color.a;
		_fillArea.color = _barColor;
	}

	IEnumerator ToggleBarAnim(float startAlpha, float endAlpha)
	{
		var startTime = Time.time;
		const float duration = 0.4f;
		float progress = 0;

		while (progress < 1)
		{
			progress = (Time.time - startTime) / duration;
			var currentColor = Mathf.Lerp(startAlpha, endAlpha, progress);
			_fillArea.color = new Color(_fillArea.color.r, _fillArea.color.g, _fillArea.color.b, currentColor);
			_backgroundArea.color = new Color(_backgroundArea.color.r, _backgroundArea.color.g, _backgroundArea.color.b, currentColor);

			yield return new WaitForEndOfFrame();
		}
		yield return null;
	}

	IEnumerator FadeTrail()
	{
		while (Trail.GetComponent<TrailRenderer>().startWidth > 0)
		{
			Trail.GetComponent<TrailRenderer>().startWidth -= 0.01f;
			yield return new WaitForEndOfFrame();
		}
		yield return null;
		Trail.SetActive(false);
	}
}
