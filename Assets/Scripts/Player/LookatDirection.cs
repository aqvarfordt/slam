﻿using UnityEngine;
using System.Collections;

public class LookatDirection : MonoBehaviour {

    public Vector3 DeltaPosition;

    Vector3 lastPosition;

    void Awake ()
    {
        lastPosition = transform.position;
    }

	void FixedUpdate () {

        DeltaPosition = transform.position - lastPosition;
        lastPosition = transform.position;
	}

    void Update ()
    {
        transform.LookAt(DeltaPosition);
    }
}
