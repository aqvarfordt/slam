﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class PlayerController : PlayerEngine
{
	void Update()
	{
		//if (PlayersManager.Instance.MaxPlayers >= PlayersManager.Players.Count)
		//{
		HorizontalAxis = Input.GetAxis("Horizontal1");
		VerticalAxis = Input.GetAxis("Vertical1");
		//}
		//else
		//{
		//	HorizontalAxis = Input.GetAxis("Horizontal1");
		//	VerticalAxis = Input.GetAxis("Vertical1");
		//}

		if (Input.GetButtonDown("Jump" + PlayerId))
		{
			if (CurrentEnergy < 20 || !_allowBoost)
				return;
			_allowBoost = false;

			Boost();
		}
	}
}
