﻿using UnityEngine;
using System.Collections.Generic;

public class DeathBounce : MonoBehaviour
{
	public float BounceForce;
	public float NewBounceAfter;

	private ContactPoint _lastContact;
	private Collision _lastCollision;
	private PlayerController _lastPlayerController;

	public void OnCollisionEnter(Collision collision)
	{
		if (!ColliderIsFloor(collision))
			return;

		_lastPlayerController = collision.gameObject.GetComponent<PlayerController>();
		_lastContact = collision.contacts[0];
		_lastCollision = collision;

		if (_lastPlayerController.CurrentEnergy < 30)
		{
			PlayersManager.Instance.PlayerDeath(_lastPlayerController.PlayerId);
			return;
		}
		
		PlayWallParticleEffect();
		Bounce();
	}

	void Bounce()
	{
		_lastPlayerController.ChangeEnergyLevel(-30);
		ParticleManager.Instance.PlayEffect(ParticleManager.Instance.BounceFloor, _lastContact.point);
		StartCoroutine(AudioManager.Instance.SlamDistort(_lastCollision.gameObject.GetComponent<PlayerController>().NormalizedVelocity));
		_lastCollision.rigidbody.AddForce(Vector3.up * BounceForce);
	}

	void PlayWallParticleEffect()
	{
		var hitParticles = Instantiate(ParticleManager.Instance.ParticleTest);

		// Emitting burst particles instead of regulating emission with emission rate
		// TODO: Instantiate particle system as a class var instead of making multiple accessor calls
		hitParticles.transform.GetChild(0).GetComponent<ParticleSystem>().emissionRate = 0;
		hitParticles.transform.GetChild(0).GetComponent<ParticleSystem>().Emit(10 * (int)_lastCollision.relativeVelocity.magnitude);

		// Basing particle start speed on the collision magnitude
		// TODO: Experiment with the start size and other kewl variables, maybe add a touch of reduced particle lifetime for small collisions
		hitParticles.transform.GetChild(0).GetComponent<ParticleSystem>().startSpeed = -1 + (_lastCollision.relativeVelocity.magnitude / 3);
		hitParticles.transform.position = new Vector3(_lastContact.point.x, _lastContact.point.y, _lastContact.point.z);
		hitParticles.transform.rotation = Quaternion.LookRotation(-_lastContact.normal);
	}

	static bool ColliderIsFloor(Collision collision)
	{
		for (int i = 0; i < collision.contacts.Length; i++)
		{
			ContactPoint contactPoint = collision.contacts[i];
			if (contactPoint.thisCollider.name == "Floor")
				return true;
		}
		return false;
	}
}

