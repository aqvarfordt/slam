﻿using UnityEngine;
using System.Collections;

public class ParticleLightSpark : MonoBehaviour
{

    public float LifeTime;
    public float FadeRate;

    private float _lightIntensityBase;
    private float _lightIntensityMin;
    private float _lightIntensityMax;

    private Light _pointLight;

    void Awake()
    {
        LifeTime = GetComponent<ParticleSystem>().duration;
        _pointLight = GetComponent<Light>();
        _lightIntensityBase = _pointLight.intensity;
        StartCoroutine(FadeLight());
    }

    IEnumerator FadeLight ()
    {
        while (_lightIntensityBase > 0)
        {
            _lightIntensityBase -= FadeRate;
            _pointLight.intensity = _lightIntensityBase;
            yield return new WaitForFixedUpdate();
        }

        yield return null;
    }

}
